"use client";
import { ExternalLinkIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  Center,
  Heading,
  Input,
  Link,
  Select,
  Table,
  TableContainer,
  Tbody,
  Td,
  Tfoot,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";

interface Presentation {
  id: number;
  person: string;
  link: string;
  subject: string;
}

export default function Page() {
  const [presentations, setPresentations] = useState<Presentation[]>([]);
  const [sortedPresentations, setSortedPresentations] = useState<
    Presentation[]
  >([]);
  const [sortOrder, setSortOrder] = useState<"asc" | "desc">("asc");

  const handleSort = (column: keyof Presentation) => {
    const sortedData = [...presentations];
    sortedData.sort((a, b) => {
      if (sortOrder === "asc") {
        return a[column].toString().localeCompare(b[column].toString());
      } else {
        return b[column].toString().localeCompare(a[column].toString());
      }
    });
    setSortedPresentations(sortedData);
    setSortOrder(sortOrder === "asc" ? "desc" : "asc");
  };

  useEffect(() => {
    fetch("http://localhost:3001/presentations")
      .then((res) => res.json())
      .then((data: Presentation[]) => {
        setPresentations(data);
        setSortedPresentations([...data]); // Initialize sortedPresentations
      });
  }, []);

  const validateForm = async (event: React.FormEvent) => {
    event.preventDefault();
    console.log("1");
    const form = event.currentTarget as HTMLFormElement;
    const data = {
      person: form.querySelector<HTMLInputElement>('[name="person"]').value,
      link: form.querySelector<HTMLInputElement>('[name="link"]').value,
      subject: form.querySelector<HTMLInputElement>('[name="subject"]').value,
    };

    const JSONdata = JSON.stringify(data);

    const endpoint = "http://localhost:3001/createPresentation";
    console.log("3");

    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        cors: "no-cors",
      },
      body: JSONdata,
    };

    const response = await fetch(endpoint, options);

    if (response.ok) {
      const result = await response.json();
      console.log("test : ", result);
      console.log("id ? : ", result.id);
      window.location.reload();
    } else {
      console.error("Error sending data.");
    }
  };

  const handleDelete = async (id: number) => {
    const endpoint = `http://localhost:3001/deletepresentation/${id}`;

    const options = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        cors: "no-cors",
      },
    };

    const response = await fetch(endpoint, options);

    if (response.ok) {
      setPresentations((prevPresentations) =>
        prevPresentations.filter((presentation) => presentation.id !== id)
      );
      window.location.reload();
    } else {
      console.error("Error deleting presentation.");
    }
  };

  return (
    <>
      <Box
        bg="#fafafa"
        w="700px"
        borderWidth="1px"
        borderRadius="lg"
        mx="auto"
        pl="5"
        pr="5"
        mt="5"
        pt="5"
      >
        <Center>
          <Heading>Formulaire - Veille Websurg</Heading>
        </Center>
        <form onSubmit={validateForm}>
          <label htmlFor="person">Personne</label>
          <Select placeholder="Choisissez la personne" name="person">
            <option value="Jean-Baptiste Stéphan">Jean-Baptiste Stéphan</option>
            <option value="Guillaume Tavernier">Guillaume Tavernier</option>
            <option value="Stéphane Becker">Stéphane Becker</option>
            <option value="Théo Ludwig">Théo Ludwig</option>
            <option value="Océane Druenne">Océane Druenne</option>
          </Select>
          <label htmlFor="link">Lien</label>
          <Input placeholder="Lien" name="link" />
          <label htmlFor="subject">Sujet</label>
          <Input placeholder="Sujet" name="subject" />
          <Center>
            <Button
              colorScheme="blue"
              mt="1"
              mb="1"
              type="submit"
              className="Button"
            >
              Envoyer
            </Button>
          </Center>
        </form>
      </Box>
      <Box mt="5">
        <TableContainer>
          <Table size="sm">
            <Thead>
              <Tr>
                <Th fontSize="xl" onClick={() => handleSort("person")}>
                  Person
                </Th>
                <Th fontSize="xl" onClick={() => handleSort("subject")}>
                  Sujet
                </Th>
                <Th fontSize="xl">Supprimer</Th>
              </Tr>
            </Thead>
            <Tbody>
              {sortedPresentations.map((presentation) => (
                <Tr key={presentation.id} mb="1">
                  <Td fontSize="lg">{presentation.person}</Td>
                  <Td fontSize="lg">
                    <Link href={presentation.link} isExternal>
                      {presentation.subject} <ExternalLinkIcon mx="2px" />
                    </Link>
                  </Td>
                  <Td>
                    <Button
                      colorScheme="red"
                      onClick={() => handleDelete(presentation.id)}
                    >
                      Supprimer
                    </Button>
                  </Td>
                </Tr>
              ))}
            </Tbody>
            <Tfoot>
              <Tr>
                <Th fontSize="xl" onClick={() => handleSort("person")}>
                  Person
                </Th>

                <Th fontSize="xl">Sujet</Th>
                <Th fontSize="xl" onClick={() => handleSort("subject")}>
                  Supprimer
                </Th>
              </Tr>
            </Tfoot>
          </Table>
        </TableContainer>
      </Box>
    </>
  );
}
