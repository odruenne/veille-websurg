const presentation_ctrl = require("../controllers/presentation_ctrl");

module.exports = [
  {
    url: "/presentations",
    method: "get",
    func: presentation_ctrl.get_all,
  },
  {
    url: "/createPresentation",
    method: "post",
    func: presentation_ctrl.create,
  },
  {
    url: "/presentation/:presentation_id",
    method: "get",
    func: presentation_ctrl.get_by_id,
  },
  {
    url: "/deletepresentation/:presentation_id",
    method: "delete",
    func: presentation_ctrl.delete_by_id,
  },
];
