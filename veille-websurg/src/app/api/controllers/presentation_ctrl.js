const db = require("../models");

module.exports = {
  get_all: (req, res, next) => {
    return db.Presentation.findAll()
      .then((presentations) => {
        res.json(presentations);
      })
      .catch(next);
  },

  create: (req, res, next) => {
    return db.Presentation.create(req.body)
      .then((presentation) => {
        res.statusCode = 201;
        res.json(presentation);
      })
      .catch(next);
  },

  get_by_id: (req, res, next) => {
    const { presentation_id } = req.params;
    return db.Presentation.findByPk(presentation_id)
      .then((presentation) => {
        if (presentation == null) {
          throw { status: 404, message: "Requested Presentation not found" };
        }
        res.json(presentation);
      })
      .catch(next);
  },

  delete_by_id: (req, res, next) => {
    const { presentation_id } = req.params;

    return db.Presentation.findByPk(presentation_id)
      .then((presentation) => {
        if (!presentation) {
          throw { status: 404, message: "Requested Presentation not found" };
        }

        return presentation.destroy();
      })
      .then(() => {
        res.statusCode = 204;
        res.end();
      })
      .catch(next);
  },
};
