const sequelize = require("sequelize");
const { Sequelize, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  class Presentation extends Sequelize.Model {
    static associate(db) {}
  }

  Presentation.init(
    {
      person: {
        type: DataTypes.ENUM(
          "Jean-Baptiste Stéphan",
          "Stéphane Becker",
          "Théo Ludwig",
          "Guillaume Tavernier",
          "Océane Druenne"
        ),
        defaultValue: "Jean-Baptiste Stéphan",
      },
      link: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      subject: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "Presentation",
      tableName: "Presentation",
    }
  );

  return Presentation;
};
